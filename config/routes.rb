Rails.application.routes.draw do
  root "sessions#new"
  get    '/introduce',   to: 'dashboard#introduce'
  get    '/login',       to: 'sessions#new'
  post   '/login',       to: 'sessions#create'
  delete '/logout',      to: 'sessions#destroy'
  put 'users/:id/token', to: "users#token"
  resources :tournaments do
    resources :clubs, only: %i[ index new create destroy ], controller: "tournaments/clubs"
  end
  resources :matches, only: :update
  resources :dashboard, only: :index
  resources :clubs do
    resources :players, controller: "clubs/players"
    resources :users, controller: "clubs/users"
  end

  mount LetterOpenerWeb::Engine, at: '/letters' if Rails.env.development?
end
