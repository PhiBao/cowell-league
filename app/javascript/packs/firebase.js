import "bootstrap";

const firebaseConfig = {
  apiKey: "AIzaSyBbKrUewqj-5_CA3RA-4aRLIALQidEDWGI",
  authDomain: "cowellleague.firebaseapp.com",
  databaseURL: "https://cowellleague.firebaseio.com",
  projectId: "cowellleague",
  storageBucket: "cowellleague.appspot.com",
  messagingSenderId: "1082005096560",
  appId: "1:1082005096560:web:62391a4c0001d708afccb5",
  measurementId: "G-06QGVCC3T1",
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.onMessage((payload) => {
  const data = payload.data;
  $("#toast-title").html(data.title);
  $("#toast-time").html(data.time);
  $("#toast-body").html(data.message);
  $("#liveToast").toast("show");
});

window.connectFirebase = (id) => {
  messaging
    .getToken({
      vapidKey:
        "BFC2T23zpL9m0L9nHfjpKEG5L6cfhpYcr8PzuENQb_YHf6-2DCKde0lLg3bSOBKs1-ppA-OgZxCDLD4guKBRPqE",
    })
    .then((currentToken) => {
      if (currentToken) {
        console.log(currentToken);
        sendTokenToServer(id, currentToken);
      } else {
        console.log(
          "No registration token available. Request permission to generate one."
        );
        setTokenSentToServer(false);
      }
    })
    .catch((err) => {
      console.log("An error occurred while retrieving token. ", err);
      setTokenSentToServer(false);
    });
};

const putData = async (id, dataObject) => {
  const response = await fetch(
    "https://cowell-league.herokuapp.com" + `/users/${id}/token`,
    // "http://localhost:3000" + `/users/${id}/token`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(dataObject),
    }
  );

  const data = await response.json();

  console.log("Data: ", data);
};

function sendTokenToServer(id, currentToken) {
  if (!isTokenSentToServer()) {
    const dataObject = { token: currentToken };
    putData(id, dataObject);
    setTokenSentToServer(true);
  } else {
    console.log(
      "Token already sent to server so won't send it again " +
        "unless it changes"
    );
  }
}

function isTokenSentToServer() {
  return window.localStorage.getItem("sentToServer") === "1";
}

function setTokenSentToServer(sent) {
  window.localStorage.setItem("sentToServer", sent ? "1" : "0");
}

window.deleteToken = () => {
  setTokenSentToServer(false);
};
