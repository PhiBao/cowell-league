require("@rails/ujs").start();
require("turbolinks").start();
require("@rails/activestorage").start();
require("channels");
require("bootstrap");
require("utilities/tom_select");

import "@fortawesome/fontawesome-free/css/all";
import "../stylesheets/application";
import "chartkick/chart.js";
import "direct_uploads";
import $ from "jquery";
global.$ = jQuery;

document.addEventListener("turbolinks:load", () => {
  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
  });
});
