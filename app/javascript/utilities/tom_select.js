import "tom-select";
import TomSelect from "tom-select";

document.addEventListener("turbolinks:load", () => {
  const selectInput = document.getElementById("select-clubs");
  if (selectInput) {
    new TomSelect(selectInput, {
      plugins: ["remove_button"],
      create: true,
      onItemAdd: function () {
        this.setTextboxValue("");
        this.refreshOptions();
      },
    });
  }
});
