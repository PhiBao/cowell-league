class Match < ApplicationRecord
  enum round: { round_of_16: 1, quarter_final: 2, semi_final: 3, final: 4 }
  belongs_to :tournament
  belongs_to :home, class_name: 'Club', foreign_key: 'club_one_id', optional: true 
  belongs_to :away, class_name: 'Club', foreign_key: 'club_two_id', optional: true 

  validates :club_one_score, numericality: { less_than_or_equal_to: 100,
                                             greater_than_or_equal_to: 0,
                                             only_integer: true }, on: :update
  validates :club_two_score, numericality: { less_than_or_equal_to: 100,
                                             greater_than_or_equal_to: 0,
                                             only_integer: true }, on: :update
  validates :club_one_id, presence: true, on: :update
  validates :club_two_id, presence: true, on: :update

  def is_done?
    self.club_one_score.present? && self.club_two_score.present?
  end

  def undefined
    !(self.home.present? && self.away.present?)
  end

  def won_team
    if self.club_one_score > self.club_two_score
      self.home
    else
      self.away
    end
  end

  def index
    curr_match_ids = self.tournament.matches.where(round: self.round).order(:id).pluck(:id)
    index = curr_match_ids.find_index(self.id) 
  end

  def next_match_position
    index = self.index
    games = { round_of_16: 16, quarter_final: 24, semi_final: 28, final: 30 }
    games[:"#{self.round}"] + (index / 2) * 2 + index % 2
  end

  def team_position
    games = { round_of_16: 0, quarter_final: 16, semi_final: 24, final: 28 }
    position = games[:"#{self.round}"] + self.index * 2
    if self.club_one_score > self.club_two_score
      {
        won: position,
        lost: position + 1
      }
    else
      {
        won: position + 1,
        lost: position
      }
    end
  end

  def disabled_position
    if self.round_of_16? 
      return -1;
    end
    games = { quarter_final: 0, semi_final: 16, final: 24 }
    games[:"#{self.round}"] + index * 4
  end

  def update_flow params
    if self.update(params)
      if !self.final?
        index = self.index
        next_match_ids = self.tournament.matches.where(round: Match.rounds["#{self.round}"] + 1).order(:id).pluck(:id)
        next_match = Match.find(next_match_ids[index / 2])
        if index % 2 == 0
          next_match.update_column(:club_one_id, self.won_team.id)
        else
          next_match.update_column(:club_two_id, self.won_team.id)
        end
      end
      true
    else
      false
    end
  end

  def disabled?
    self.tournament
        .matches
        .where("(club_one_id = ? OR club_two_id = ? OR club_one_id = ? OR club_two_id = ?) AND round = ?",
                self.home&.id, self.home&.id, self.away&.id, self.away&.id, Match.rounds["#{self.round}"] + 1)
        .first&.is_done? == true
  end

  def notifications
    res = []
    res << home.users.pluck(:token)
    res << away.users.pluck(:token)
    res.uniq.compact.flatten
  end

  def message
    "#{home.name} vs #{away.name} updated the score to #{club_one_score}-#{club_two_score}"
  end
end
