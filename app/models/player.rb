class Player < ApplicationRecord
  include Newest
  paginates_per 10
  belongs_to :club, counter_cache: true

  validates :email, presence: true, uniqueness: true, format: VALID_EMAIL_REGEX
  validates :name, presence: true, length: { minimum: 1, maximum: 30 }

  def send_joined_email
    PlayerMailer.with(player: self).club_joined.deliver_later
  end
end
