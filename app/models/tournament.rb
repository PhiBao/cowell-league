class Tournament < ApplicationRecord
  include Newest
  paginates_per 15
  enum status: { newly: 1, started: 2, finished: 3 }
  
  has_many :tournament_clubs, dependent: :destroy
  has_many :clubs, through: :tournament_clubs
  has_many :matches, dependent: :destroy

  validates :name, presence: true, length: { minimum: 5, maximum: 50 }
  validates :start_date, presence: true
  validate :validate_clubs

  scope :has_user, -> (user_id) { joins(clubs: :users)
                                  .where(clubs: { users: { id: user_id } }) }

  def validate_clubs
    errors.add(:tournament, "has a maximum of 16 clubs") if clubs.size > 16
  end
  
  def tournament_clubs_attributes=(array)
    array.each do |item|
      self.tournament_clubs.find_or_create_by(item)
    end
  end

  def can_start?
    return self.newly? && self.clubs.size == 16
  end

  def can_finish?
    return false unless self.started?
    self.matches.each { |match| return false if !match.is_done? }
    true
  end

  def start params
    if self.update(params)
      club_ids = self.clubs.pluck(:id)
      time = self.start_date
      club_ids.shuffle.each_slice(2) do |one, two|
        Match.create!(club_one_id: one,
                      club_two_id: two,
                      tournament: self,
                      round: 1,
                      time: time)
        time = time + 1.day
      end
      4.times do
        Match.create!(
          tournament: self,
          round: 2,
          time: time)
        time = time + 1.day
      end
      2.times do
        Match.create!(
          tournament: self,
          round: 3,
          time: time)
        time = time + 1.day
      end
      Match.create!(
        tournament: self,
        round: 4,
        time: time + 3.days)
      true
    else
      false
    end
  end

  def not_done_matches
    self.matches.reject { |match| match.undefined || match.is_done? }.take(5)
  end
end
