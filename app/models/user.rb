class User < ApplicationRecord
  include Newest
  enum admin: { admin: true, normal: false }
  attr_accessor :remember_token
  before_save   :downcase_email

  has_many :user_clubs, dependent: :destroy
  has_many :clubs, through: :user_clubs
  
  validates :name, presence: true, length: { minimum: 1, maximum: 30 }
  validates :email, presence: true, uniqueness: true, format: VALID_EMAIL_REGEX
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_blank: true

  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update(remember_digest: User.digest(remember_token))
    remember_digest
  end

  # Returns a session token to prevent session hijacking.
  # We reuse the remember digest for convenience.
  def session_token
    remember_digest || remember
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Forgets a user.
  def forget
    update(remember_digest: nil)
  end

  private

    # Converts email to all lowercase.
    def downcase_email
      self.email = email.downcase
    end
end
