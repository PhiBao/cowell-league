class Club < ApplicationRecord
  include Newest
  paginates_per 10
  
  has_one_attached :avatar
  has_many :user_clubs, dependent: :destroy
  has_many :users, through: :user_clubs
  has_many :players, dependent: :destroy
  has_many :tournament_clubs, dependent: :destroy
  has_many :tournaments, through: :tournament_clubs
  has_many :home_matches, class_name: 'Match', 
                            foreign_key: 'club_one_id', dependent: :destroy
  has_many :away_matches, class_name: 'Match',
                            foreign_key: 'club_two_id', dependent: :destroy

  validates :name, presence: true, length: { minimum: 1, maximum: 100 }
  validates :avatar, dimension: { width: { min: 100, max: 2400 } },
                     content_type: [:png, :jpg, :jpeg, :gif],
                     size: { less_than: 5.megabytes,
                             message: 'is not given between size' }

  scope :valid, -> { where('players_count > 7') }

  def can_delete?
    self.tournaments.size == 0
  end
end
