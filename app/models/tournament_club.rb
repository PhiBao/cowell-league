class TournamentClub < ApplicationRecord
  belongs_to :tournament
  belongs_to :club
end
