class DashboardData < ApplicationService
  attr_reader :data, :page, :user_id

  def initialize(page, user_id, data = {})
    @data = data
    @page = page
    @user_id = user_id
  end

  def call
    if User.find(@user_id)&.admin?
      started = Tournament.includes(matches: [:home, :away]).started.page(@page).per(3)
      newly = Tournament.newly.last(3)
      finished = Tournament.finished.last(3)
      chart = User.normal.group_by_day(:created_at).count
    else
      tournament = Tournament.includes(matches: [:home, :away]).has_user(@user_id)
                             .started.page(@page).per(3)
      started = tournament.started.page(@page).per(3)
      newly = tournament.newly.last(3)
      finished = tournament.finished.last(3)
      chart = Player.joins(club: :users)
                    .where(club: { users: { id: @user_id } })
                    .group_by_day(:created_at).count
    end

    @data = {
      started: started,
      newly: newly,
      finished: finished,
      chart: chart
    }
  end
end
