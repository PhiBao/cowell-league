class ApplicationMailer < ActionMailer::Base
  default from: 'Kanna <kanna@cowell.com>'
  layout 'mailer'
end
