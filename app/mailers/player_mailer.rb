class PlayerMailer < ApplicationMailer
  def club_joined
    @player = params[:player]
    
    mail(
      to: email_address_with_name(@player.email, @player.name),
      subject: "Club joined"
    )
  end
end
