module NotificationHelper
  require 'fcm'

  def self.push_notification(tokens = [], match)
    fcm = FCM.new(ENV['SERVER_KEY'])
    options = {
      notification: {
        title: "You have a new notification",
        body: "Your match has been updated score"
      },
      data: {
        time: ApplicationController.helpers.time_ago_in_words(match.updated_at),
        message: match.message,
        title: match.tournament.name
      }
    }
    
    response = fcm.send(tokens, options)
  end
end
