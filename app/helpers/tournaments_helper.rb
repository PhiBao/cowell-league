module TournamentsHelper
  def date_formatter date
    date.strftime("%d-%m-%Y %k:%M:%S")
  end

  def bold_score one, two
    if one.to_i > two.to_i
      "fw-bold"
    else
      ""
    end
  end

  def avatar_url club
    if club&.avatar&.attached?
      url_for(club.avatar)
    elsif club.present?
      image_url("no-logo.png")
    end
  end
end
