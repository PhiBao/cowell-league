class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :token
  before_action :get_user, only: :token

  def token
    if @user.update(token_param)
      render json: { ok: true }, status: :ok
    else
      render json: { ok: false }, status: :unprocessable_entity
    end
  end

  private

    def get_user
      @user = User.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      respond_to do |format|
        format.html { redirect_to root_url, flash: { danger: "User not found." } }
        format.json { head :not_found }
      end
    end

    def token_param
      params.require(:user).permit(:token)
    end
end
