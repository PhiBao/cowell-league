class TournamentsController < ApplicationController
  before_action :logged_in_user
  before_action :admin_user, only: %i[ new create edit update destroy ]
  before_action :get_tournament, only: %i[ show edit update destroy ]
  before_action :correct_user, only: :show

  def index
    page = params[:page] || 1
    if current_user.admin?
      @q = Tournament.newest.ransack(params[:q])
    else
      @q = Tournament.has_user(current_user.id).newest.ransack(params[:q])
    end
    @tournaments = @q.result.page(page)

    render do |format|
      format.html { render :index }
      format.json { render json: Tournament.all.includes(clubs: :players), status: :ok }
    end

  end

  def show
  end

  def new
    @tournament = Tournament.new
  end

  def create
    @tournament = Tournament.new(tournament_params)

    respond_to do |format|
      if @tournament.save
        format.html { redirect_to tournaments_url, flash: { success: "Tournament was successfully created." } }
        format.json { head :no_content }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tournament.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end
  
  def update
    if @tournament.can_start? && tournament_params[:status] == "started" && @tournament.start(tournament_params)
      respond_to do |format|
        format.html { redirect_to tournaments_url, flash: { success: "Tournament was successfully updated." } }
        format.json { head :no_content }
      end
    elsif @tournament.can_finish? && tournament_params[:status] == "finished" && @tournament.update(tournament_params)
      respond_to do |format|
        format.html { redirect_to tournaments_url, flash: { success: "Tournament was successfully updated." } }
        format.json { head :no_content }
      end
    else
      flash.now[:danger] = "Tournament updated failed"
      respond_to do |format|
        format.html { render :edit, status: :unprocessable_entity }
        format.json { head :no_content }
      end
    end
  end

  def destroy
    if @tournament.newly? && @tournament.destroy
      respond_to do |format|
        format.html { redirect_to tournaments_url, flash: { success: "Tournament was successfully destroyed." } }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to tournaments_url, flash: { danger: "Tournament destroyed failed." } }
        format.json { head :unprocessable_entity }
      end
    end
  end

  private

    def get_tournament
      @tournament = Tournament.includes(matches: [:home, :away]).find(params[:id])
    rescue ActiveRecord::RecordNotFound
      respond_to do |format|
        format.html { redirect_to root_url, flash: { danger: "Tournament not found." } }
        format.json { head :not_found }
      end
    end

    def tournament_params
      params.require(:tournament).permit(:name, :start_date, :status)
    end

    # Confirms the correct user.
    def correct_user
      unless current_user.admin? || Tournament.has_user(current_user.id).include?(@tournament)
        redirect_to dashboard_index_url, flash: { danger: "You do not have the right to access." }
      end
    end
end
