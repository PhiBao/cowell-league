class ApplicationController < ActionController::Base
  include SessionsHelper

  private

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        redirect_to root_url, flash: { danger: "Please log in." } 
      end
    end

    def admin_user
      unless current_user&.admin?
        redirect_to dashboard_index_url, flash: { danger: "You do not have the right to access." }
      end
    end
end
