class DashboardController < ApplicationController
  before_action :logged_in_user, only: :index
  
  def index
    page = params[:page] || 1
    @data = DashboardData.call(page, current_user&.id)
  end

  def introduce
  end
end
