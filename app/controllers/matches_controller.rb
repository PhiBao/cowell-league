class MatchesController < ApplicationController
  include NotificationHelper
  before_action :logged_in_user
  before_action :admin_user
  before_action :get_match

  def update
    respond_to do |format|
      if @match.update_flow(match_params)
        NotificationHelper.push_notification(@match.notifications, @match)
        format.html { redirect_to tournament_url(@match.tournament), 
                      flash: { success: "Match was successfully updated." } }
        format.json { render json: { ok: true }, status: :ok }
        format.js
      else
        format.html { redirect_to tournament_url(@match.tournament), status: :unprocessable_entity,
                      flash: { danger: "The match must have both teams.
                                        Score must be a number and less than 100." }}
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    def match_params
      params.require(:match).permit(:club_one_score, :club_two_score)
    end

    def get_match
      @match = Match.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      respond_to do |format|
        format.html { redirect_to root_url, flash: { danger: "Match not found." } }
        format.json { head :not_found }
      end
    end
end
