module Clubs
  class PlayersController < ApplicationController
    before_action :logged_in_user
    before_action :set_club
    before_action :correct_user, only: %i[ new create edit update destroy ]
    before_action :set_player, only: %i[ edit update destroy ]

    def index
      page = params[:page] || 1
      @q = @club.players.newest.ransack(params[:q])
      @players = @q.result.page(page)
    end
  
    def new
      @player = @club.players.build
    end
  
    def create
      @player = @club.players.build(player_params)
      respond_to do |format|
        if @player.save
          @player.send_joined_email
          format.html { redirect_to club_players_url(@club), flash: { success: "Player was successfully created." } }
          format.json { render :show, status: :created, location: @player }
        else
          format.html { render :new, status: :unprocessable_entity }
          format.json { render json: @player.errors, status: :unprocessable_entity }
        end
      end
    end

    def edit
    end
  
    # PATCH/PUT /palyers/1 or /palyers/1.json
    def update
      respond_to do |format|
        if @player.update(player_params)
          format.html { redirect_to club_players_url(@club), flash: { success: "Player was successfully updated." } }
          format.json { render :show, status: :ok, location: @player }
        else
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @player.errors, status: :unprocessable_entity }
        end
      end
    end
  
    
    def destroy
      @player.destroy
      respond_to do |format|
        format.html { redirect_to club_players_url(@club), flash: { success: "Player was successfully destroyed." } }
        format.json { head :no_content }
      end
    end
  
    private

      def set_club
        @club = Club.includes(:users).find(params[:club_id])
      rescue ActiveRecord::RecordNotFound
        # trả về trang root_url khi có ngoại lệ
        respond_to do |format|
          format.html { redirect_to root_url, flash: { danger: "club not found." } }
          format.json { head :not_found }
        end
      end
   
      def set_player     
        @player = @club.players.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        respond_to do |format|
          format.html { redirect_to club_players_url(@club), flash: { danger: "player not found." } }
          format.json { head :not_found }
        end
      end
  
      # Only allow a list of trusted parameters through.
      def player_params
        params.require(:player).permit(:name, :email, :club_id)
      end

      # Confirms the correct user.
      def correct_user
        unless current_user.admin? || current_user.clubs.include?(@club)
          redirect_to dashboard_index_url, flash: { danger: "You do not have the right to access." }
        end
      end
  end    
end
