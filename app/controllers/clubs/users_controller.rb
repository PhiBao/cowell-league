module Clubs
  class UsersController < ApplicationController
    before_action :logged_in_user
    before_action :admin_user, only: :destroy
    before_action :set_club
    before_action :correct_user, only: %i[ new create edit update ]
    before_action :set_user, only: %i[ edit update destroy ]

    # GET /users or /users.json
    def index
      page = params[:page] || 1
      @q = @club.users.newest.ransack(params[:q])
      @users = @q.result.page(page).per(10)
    end
   
    # GET /users/new
    def new   
      @user = User.new
    end

    # POST /users or /users.json
    def create
      @user = User.create(user_params)
      if @user.valid?
        @club.users << @user
        respond_to do |format| 
          format.html { redirect_to club_users_url(@club), flash: { success: "User was successfully created." } } 
          format.json { render :show, status: :created, location: @user }
        end
      else
        respond_to do |format| 
          format.html { render :new, status: :unprocessable_entity }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end

    def edit
    end
  
    # PATCH/PUT /users/1 or /users/1.json
    def update
      respond_to do |format|
        if @user.update(user_params)
          format.html { redirect_to club_users_url(@club), flash: { success: "User was successfully updated." } }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      @user.destroy
      
      respond_to do |format|
        format.html { redirect_to club_users_url(@club), flash: { success: "User was successfully destroyed." } }
        format.json { head :no_content }
      end
    end

    private
    
      def set_club
        @club = Club.includes(:users).find(params[:club_id])
      rescue ActiveRecord::RecordNotFound
        # trả về trang root_url khi có ngoại lệ
        respond_to do |format|
          format.html { redirect_to root_url, flash: { danger: "club not found." } }
          format.json { head :not_found }
        end
      end
  
      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = @club.users.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        respond_to do |format|
          format.html { redirect_to club_users_url(@club), flash: { danger: "User not found." } }
          format.json { head :not_found }
        end
      end
    
      # Only allow a list of trusted parameters through.
      def user_params
        params.require(:user).permit(:name, :email, :password, :password_confirmation)
      end

      # Confirms the correct user.
      def correct_user
        unless current_user.admin? || current_user.clubs.include?(@club)
          redirect_to dashboard_index_url, flash: { danger: "You do not have the right to access." }
        end
      end
  end
end
