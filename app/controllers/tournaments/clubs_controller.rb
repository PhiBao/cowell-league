module Tournaments
  class ClubsController < ApplicationController
    before_action :logged_in_user
    before_action :admin_user
    before_action :set_tournament
    before_action :valid_clubs, only: %i[ new create ]
    before_action :set_club, only: :destroy

    def index
      page = params[:page] || 1
      @clubs = @tournament.clubs.page(page).per(8)
    end

    def new
    end
  
    def create
      respond_to do |format|
        if @tournament.update(tournament_params)
          format.html { redirect_to tournament_clubs_url(@tournament), flash: { success: "Add clubs successfully." } }
          format.json { render json: { ok: true }, status: :created}
        else
          flash.now[:danger] = "Tournament has maximum 16 clubs."
          format.html { 
            render :new, status: :unprocessable_entity
          }
          format.json { head :unprocessable_entity }
        end
      end
    end

    def destroy
      @tournament.clubs.delete(@club)

      respond_to do |format|
        format.html { redirect_to tournament_clubs_path(@tournament), flash: { success: "Club was successfully destroyed." } }
        format.json { head :no_content }
      end
    end

    private

      def set_tournament
        @tournament = Tournament.find(params[:tournament_id])
      rescue ActiveRecord::RecordNotFound
        respond_to do |format|
          format.html { redirect_to root_url, flash: { danger: "Tournament not found." } }
          format.json { head :not_found }
        end
      end

      def set_club
        @club = @tournament.clubs.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        respond_to do |format|
          format.html { redirect_to root_url, flash: { danger: "Club not found." } }
          format.json { head :not_found }
        end
      end  

      def tournament_params
        params.require(:tournament).permit(tournament_clubs_attributes: []).tap do |attrs| 
          attrs['tournament_clubs_attributes'] = attrs['tournament_clubs_attributes']
                                                 .compact_blank.map { |id| { club_id: id.to_i }.to_hash }
        end
      end

      def valid_clubs
        @options = Club.valid.reject { |club| @tournament.clubs.include?(club) }
                           .pluck(:name, :id)
      end
  end
end
