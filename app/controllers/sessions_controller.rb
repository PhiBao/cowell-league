class SessionsController < ApplicationController

  def new
    if logged_in?
      respond_to do |format|
        format.html { redirect_to dashboard_index_url }
        format.json { head :no_content }
      end
    end
    @errors = { email: "", password: "" }
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      forwarding_url = session[:forwarding_url]
      reset_session
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      log_in user
      respond_to do |format|
        format.html { redirect_to forwarding_url || dashboard_index_url, flash: { info: "Welcome to Cowell League." } }
        format.json { head :no_content }
      end
    else
      @errors = { email: "", password: "" }

      unless user.present?
        @errors[:email] = "Invalid email"
      else
        @errors[:password] = "Invalid password"
      end
      respond_to do |format|
        format.html { render :new }
        format.json { head :unprocessable_entity }
      end
    end
  end

  def destroy
    log_out if logged_in?
    respond_to do |format|
      format.js
      format.html { redirect_to root_url }
      format.json { head :no_content }
    end
  end
end
