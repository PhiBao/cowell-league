class ClubsController < ApplicationController
    before_action :logged_in_user
    before_action :admin_user, only: %i[ new create destroy ]
    before_action :set_club, only: %i[ edit update destroy ]
    before_action :correct_user, only: %i[ edit update ]

    # GET /clubs or /clubs.json
    def index
      page = params[:page] || 1
      @q = Club.newest.ransack(params[:q])
      @clubs = @q.result.page(page)

    end
  
    # GET /clubs/1 or /clubs/1.json
 
    # GET /clubs/new
    def new
      @club = Club.new
    end
  
    # GET /clubs/1/edit
    def edit
    end
  
    # POST /clubs or /clubs.json
    def create
      @club = Club.new(club_params)
  
      respond_to do |format|
        if @club.save
          format.html { redirect_to clubs_url, flash: { success: "Club was successfully created." } }
          format.json { render :show, status: :created, location: @club }
        else
          format.html { render :new, status: :unprocessable_entity }
          format.json { render json: @club.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # PATCH/PUT /clubs/1 or /clubs/1.json
    def update
      respond_to do |format|
        if @club.update(club_params) 
          format.html { redirect_to clubs_url, flash: { success: "Club was successfully updated." } }
          format.json { render :show, status: :ok, location: @club }
        else
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @club.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # DELETE /clubs/1 or /clubs/1.json
    def destroy
      @club.destroy
      
      respond_to do |format|
        format.html { redirect_to clubs_url, flash: { success: "Club was successfully destroyed." } }
        format.json { head :no_content }
      end
    end
  
    private

      def set_club
        @club = Club.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        respond_to do |format|
          format.html { redirect_to clubs_url, flash: { danger: "Club not found." } }
          format.json { head :no_content }
        end
      end
  
      def club_params
        params.require(:club).permit(:name , :avatar)
      end

      # Confirms the correct user.
      def correct_user
        unless current_user.admin? || current_user.clubs.include?(@club)
          redirect_to dashboard_index_url, flash: { danger: "You do not have the right to access." }
        end
      end
end
