UserClub.delete_all
User.delete_all
Player.delete_all
TournamentClub.delete_all
Match.delete_all
Tournament.delete_all
Club.delete_all

User.create!(name: Faker::Name.unique.name,
  email: "admin@cowell.com",
  password: "password",
  password_confirmation: "password",
  admin: true)

tournament = Tournament.create!(name: "World Cup", start_date: "22/11/2022", status: "started")  

16.times do |n|
  club = Club.create!(name: Faker::WorldCup.unique.team)
  user = User.create!(name: Faker::Name.name,
               email: "example-#{n+1}@cowell.com",
               password: "password",
               password_confirmation: "password")
  UserClub.create!(user: user, club: club)
  TournamentClub.create!(tournament: tournament, club: club)
end

8.times do |n|
  club = Club.create!(name: Faker::WorldCup.unique.team)
  user = User.create!(name: Faker::Name.name,
               email: "example-#{n+17}@cowell.com",
               password: "password",
               password_confirmation: "password")
  UserClub.create!(user: user, club: club)
end

clubs = Club.all
20.times do
  clubs.each { |club| club.players.create!(name: Faker::FunnyName.name, email: Faker::Internet.unique.email) }
end

club_ids = clubs.first(16).pluck(:id)
time = tournament.start_date

club_ids.shuffle.each_slice(2) do |one, two|
  Match.create!(club_one_id: one,
                club_two_id: two,
                tournament: tournament,
                round: 1,
                time: time)
  time = time + 1.day
end

4.times do
  Match.create!(
    tournament: tournament,
    round: 2,
    time: time)
  time = time + 1.day
end

2.times do
  Match.create!(
    tournament: tournament,
    round: 3,
    time: time)
  time = time + 1.day
end

Match.create!(
  tournament: tournament,
  round: 4,
  time: time + 3.days)
