class CreateMatches < ActiveRecord::Migration[6.1]
  def change
    create_table :matches do |t|
      t.integer :club_one_id
      t.integer :club_two_id
      t.date :time
      t.integer :round
      t.integer :club_one_score
      t.integer :club_two_score
      t.references :tournament, null: false, foreign_key: true

      t.timestamps
    end

    add_foreign_key :matches, :clubs, column: :club_one_id
    add_foreign_key :matches, :clubs, column: :club_two_id
  end
end
