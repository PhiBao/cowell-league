class AddPlayersCountToClubs < ActiveRecord::Migration[6.1]
  def change
    add_column :clubs, :players_count, :integer, default: 0
  end
end
