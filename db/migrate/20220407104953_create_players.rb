class CreatePlayers < ActiveRecord::Migration[6.1]
  def change
    create_table :players do |t|
      t.references :club, null: false, foreign_key: true
      t.string :email
      t.string :name

      t.timestamps
    end
  end
end
