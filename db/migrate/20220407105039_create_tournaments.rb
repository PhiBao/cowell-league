class CreateTournaments < ActiveRecord::Migration[6.1]
  def change
    create_table :tournaments do |t|
      t.string :name
      t.date :start_date
      t.integer :status, default: 1

      t.timestamps
    end
    
    add_index :tournaments, :name, unique: true
  end
end
