class CreateTournamentClubs < ActiveRecord::Migration[6.1]
  def change
    create_table :tournament_clubs do |t|
      t.references :tournament, null: false, foreign_key: true
      t.references :club, null: false, foreign_key: true

      t.timestamps
    end
  end
end
