importScripts(
  "https://www.gstatic.com/firebasejs/9.2.0/firebase-app-compat.js"
);
importScripts(
  "https://www.gstatic.com/firebasejs/9.2.0/firebase-messaging-compat.js"
);

firebase.initializeApp({
  apiKey: "AIzaSyBbKrUewqj-5_CA3RA-4aRLIALQidEDWGI",
  authDomain: "cowellleague.firebaseapp.com",
  projectId: "cowellleague",
  storageBucket: "cowellleague.appspot.com",
  messagingSenderId: "1082005096560",
  appId: "1:1082005096560:web:62391a4c0001d708afccb5",
  measurementId: "G-06QGVCC3T1",
});

const messaging = firebase.messaging();
